using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    [Header("# 카메라 Y축 이동 범위")]
    [Range(-10.0f, 10.0f)]
    public float m_MaxMoveY;
    [Range(-10.0f, 10.0f)]
    public float m_MinMoveY;

    public GameObject m_CatObject;

    /// <summary>
    /// 카메라의 이동 목표 위치를 기록할 변수입니다.
    /// </summary>
    private Vector3 _TargetPosition;

    private void Start()
    {
        _TargetPosition = transform.position;
    }

    private void Update()
    {
        // 목표 위치를 갱신합니다.
        UpdateTargetPosition();

        MoveToTargetPosition();
    }

    /// <summary>
    /// 목표 위치를 갱신합니다.
    /// </summary>
    private void UpdateTargetPosition()
    {
        Vector3 catPosition = m_CatObject.transform.position;
        _TargetPosition.y = catPosition.y;

        // Y 축 값을 m_MinMoveY ~ m_MinMoveX 사이의 값으로 설정합니다.
        _TargetPosition.y = Mathf.Clamp(_TargetPosition.y, m_MinMoveY, m_MaxMoveY);
    }

    /// <summary>
    /// 목표 위치로 이동합니다.
    /// </summary>
    private void MoveToTargetPosition()
    {
        transform.position = Vector3.Lerp(
            transform.position, 
            _TargetPosition, 0.01f);

        //transform.position = _TargetPosition;
    }




}
