using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Cat : MonoBehaviour
{
    public SpriteRenderer m_SpriteRenderer;

    public float m_MapRadius;

    public float m_MoveSpeed;

    /// <summary>
    /// 목표 위치를 저장할 변수입니다.
    /// </summary>
    private Vector3 _TargetPosition;

    /// <summary>
    /// 이동을 끝낸 시간을 기록할 변수입니다.
    /// </summary>
    private float _MoveFinishTime;

    /// <summary>
    /// 대기할 시간을 기록할 변수입니다.
    /// </summary>
    private float _WaitTimeSeconds;

    private Animator _Animator;




    private void Awake()
    {
        _Animator = GetComponent<Animator>();
    }

    private void Start()
    {
        transform.position = Vector3.zero;

        // 목표 위치를 설정합니다.
        _TargetPosition = GetRandomPosition();

    }

    private void Update()
    {
        // 목표 도달 여부
        bool isMoveFinish = (Vector3.Distance(transform.position, _TargetPosition) < 0.01f);

        // 대기 끝 여부
        bool isWaitFinished = (_MoveFinishTime + _WaitTimeSeconds) < Time.time;

        // 목표 위치에 도달했는가?
        if (isMoveFinish)
        {
            _Animator.SetBool("_IsMove", false);

            // 목표 위치에 도달한 시간을 기록합니다.
            _MoveFinishTime = Time.time;
            // Time.time : 플레이 이후 지난 시간을 반환합니다.

            // 대기할 시간을 결정합니다.
            _WaitTimeSeconds = Random.Range(1.0f, 3.0f);

            // 새로운 목표 위치를 설정합니다.
            _TargetPosition = GetRandomPosition();
        }
        // 도달하지 않은 경우
        else if (isWaitFinished)
        {
            _Animator.SetBool("_IsMove", true);

            // 방향을 얻습니다.
            Vector3 direction = GetMoveDirection();

            // 방향에 따라 스프라이트를 뒤집습니다.
            m_SpriteRenderer.flipX = direction.x < 0;

            // 목표 위치로 이동합니다.
            MoveToTarget(direction);
        }

        
    }

    /// <summary>
    /// 랜덤한 위치를 반환합니다.
    /// </summary>
    /// <returns></returns>
    private Vector3 GetRandomPosition()
    {
        // 30FPS PC
        // 프레임 사이 간격 시간 : 0.03333초
        // 캐릭터의 이동 속력 : 10
        // 10 * 0.03333 = 0.33333 * 30 = 10

        // 60FPS PC
        // 프레임 사이 간격 시간 : 0.0166666초
        // 캐릭터의 이동 속력 : 10
        // 10 * 0.0166666 = 0.16666 * 60 = 10

        // Time.deltaTime : 프레임 사이 간격 시간

        //transform.position += new Vector3(0.0f, 5.0f, 0.0f) * Time.deltaTime;

        Vector2 random = new Vector2(
            Random.Range(-m_MapRadius, m_MapRadius),
            Random.Range(-m_MapRadius, m_MapRadius));

        // 벡터의 길이
        float distance = random.magnitude;

        // 만약 생성된 점 위치가 원 밖을 벗어나는 위치라면
        if (distance > m_MapRadius)
        {
            // 벡터 정규화
            random.Normalize();

            // 위치를 재설정합니다.
            random *= m_MapRadius;
        }

        // 랜덤한 위치 반환
        return random;
    }

    /// <summary>
    /// 목표로 향하는 방향을 얻습니다.
    /// </summary>
    /// <returns></returns>
    private Vector3 GetMoveDirection()
    {
        return (_TargetPosition - transform.position).normalized;
    }

    /// <summary>
    /// 목표 위치로 이동합니다.
    /// </summary>
    private void MoveToTarget(Vector3 direction)
    {
        

        // 목표 위치까지의 거리를 확인합니다.
        float distance = Vector3.Distance(transform.position, _TargetPosition);

        if (distance < m_MoveSpeed * Time.deltaTime)
            transform.position = _TargetPosition;

        // 이동
        else transform.position += direction * m_MoveSpeed * Time.deltaTime;
    }


#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(_TargetPosition, 1);

        Gizmos.DrawWireSphere(Vector3.zero, m_MapRadius);
    }
#endif
}
